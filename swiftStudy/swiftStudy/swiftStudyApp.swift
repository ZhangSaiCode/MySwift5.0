//
//  swiftStudyApp.swift
//  swiftStudy
//
//  Created by ZhangSai on 2021/3/18.
//

import SwiftUI

@main
struct swiftStudyApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
